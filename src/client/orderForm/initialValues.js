export const initialValues = {
    name: '',
    lastName: '',
    patronymic: '',
    phoneNumber: '',
    email: '',
    cityCategory: 'Kyiv',
    shipping: 'Новая почта',
    street: '',
    house: '',
    flat: '',
    shippingTime: '',
    mailing: '',
    comment: '',
    submit: ''
}