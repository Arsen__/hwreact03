import React from 'react';
import {Formik, Form, Field} from 'formik';
import {fields} from './fields';
import './orderForm.scss'
import InputMask from 'react-input-mask';
import {initialValues, validate} from './configs'
import FormErrorMessage from './../../shared/FormErrorMessage/FormErrorMessage'

const OrderForm = ({addProduct}) => {
    const onSubmit = (values, {resetForm}) => {
        addProduct(values);
        resetForm();
    }
    const onSelect = (target) =>{
        
    }

    const cityOptions = fields.cityCategory.options.map(({value, text})=> <option value={value}>{text}</option>)
    const shippingCategory = fields.shippingCategory.options.map(({value, text, name})=> <option value={value} name={name} onClick={() => onSelect()}>{text}</option>)

    return (
        <Formik {...{initialValues, onSubmit, validate}}>
            <Form className='product-form'>
                <Field {...fields.name}/>
                <FormErrorMessage name={'name'}/>
                <Field {...fields.lastName}/>
                <FormErrorMessage name={'lastName'}/>
                <Field {...fields.patronymic}/>
                <FormErrorMessage name={'patronymic'}/>
                <InputMask mask={'+3\\80 99 999 99 99'} {...fields.phone}/>
                <FormErrorMessage name={'phone'}/>
                <Field {...fields.email}/>
                <FormErrorMessage name={'email'}/>
                <Field as='select' {...fields.cityCategory}>
                    {cityOptions}
                </Field>
                <Field as='select' {...fields.shippingCategory}>
                    {shippingCategory}
                </Field>
                <div className={`address`}>
                    <Field {...fields.street}/>
                    <FormErrorMessage name={'street'}/>
                    <Field {...fields.house}/>
                    <FormErrorMessage name={'house'}/>
                    <Field {...fields.flat}/>
                    <FormErrorMessage name={'flat'}/>
                    <Field {...fields.shippingTime}/>
                    <FormErrorMessage name={'shippingTime'}/>
                </div>
                <div className={'mailing'}>
                    <div>Mailing</div>
                    <label>Yes</label>
                    <Field {...fields.mailing} value='yes'/>
                    <label>No</label>
                    <Field {...fields.mailing} value='no'/>
                </div>
                <Field as='textarea' {...fields.comment}/>
                <Field {...fields.submit}/>
            </Form>
        </Formik>

    )



};

export default OrderForm;