import {errorsMsg} from "./errorsMsg";

export const initialValues = {
    name: '',
    lastName: '',
    patronymic: '',
    phoneNumber: '',
    email: '',
    cityCategory: 'Kyiv',
    shipping: 'Новая почта',
    street: '',
    house: '',
    flat: '',
    shippingTime: '',
    mailing: '',
    comment: '',
    submit: ''
};

export const validate = (values)=> {
    const errors = {};
    if(!values.name) {
        errors.name = "Поле обязательно к заполнению"
    }
    if(!values.lastName) {
        errors.lastName = "Поле обязательно к заполнению"
    }
    if(!values.patronymic) {
        errors.patronymic = "Поле обязательно к заполнению"
    }

    if (!values.phone){
        errors.phone = errorsMsg.phone;
    }

    if (values.phone.length > 13){
        console.log('vor')
    }

    if(!values.email) {
        errors.email = "Поле обязательно к заполнению"
    }

    if(!values.email.includes("@")) {
        errors.email = errorsMsg.email;
    }

    if(!values.street) {
        errors.street = "Поле обязательно к заполнению"
    }
    if(!values.house) {
        errors.house = "Поле обязательно к заполнению"
    }
    if(!values.flat) {
        errors.flat = "Поле обязательно к заполнению"
    }
    if(!values.shippingTime) {
        errors.shippingTime = "Поле обязательно к заполнению"
    }
    return errors;
}
