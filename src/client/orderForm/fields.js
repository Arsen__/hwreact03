export const fields = {
    name: {
        type: 'text',
        name: 'name',
        placeholder: 'Enter your name',
        required: true,
    },
    lastName: {
        type: 'text',
        name: 'lastName',
        placeholder: 'Enter your Lastname',
        required: true,
    },
    patronymic: {
        type: 'text',
        name: 'patronymic',
        placeholder: 'Enter your patronymic',
        required: true,
    },
    phone: {
        type: 'text',
        name: 'phone',
        placeholder: 'Enter your phone number',
        required: true,
    },
    email: {
        type: 'text',
        name: 'email',
        placeholder: 'Enter your email',
        required: true,
    },
    cityCategory: {
        name: 'city-category',
        options: [
            {
            value: 'Kyiv',
            text: 'Kyiv',
                name: 'Kyiv',
            },
            {
                value: 'Kharkov',
                text: 'Kharkov',
                name: 'Kharkov',
            },
            {
                value: 'Dnipro',
                text: 'Dnipro',
                name: 'Dnipro',
            },
            {
                value: 'Odesa',
                text: 'Odesa',
                name: 'Odesa',
            },
            {
                value: 'Lviv',
                text: 'Lviv',
                name: 'Lviv',
            }],
    },
    shippingCategory: {
        name: 'shipping-category',
        options: [
            {
                value: 'Новая почта',
                text: 'Новая почта',
                name: 'nov-p',
            },
            {
                value: 'Курьерская доставка',
                text: 'Курьерская доставка',
                name: 'courier',
            }
        ]
    },
    street: {
        type: 'text',
        name: 'street',
        placeholder: 'Enter your address street',
        required: true,
    },
    house: {
        type: 'text',
        name: 'house',
        placeholder: 'Enter your address house',
        required: true,
    },
    flat: {
        type: 'text',
        name: 'flat',
        placeholder: 'Enter your address flat',
        required: true,
    },
    shippingTime: {
        type: 'text',
        name: 'shippingTime',
        placeholder: 'Enter your shippingTime',
        required: true,
    },
    mailing: {
        required: true,
        type: 'radio',
        name: 'discount',
    },
    comment: {
        type: 'text',
        name: 'comment',
        placeholder: 'Your Comment',
        className: 'comment'
    },
    submit: {
        type: 'submit',
        name: 'submit',
        value: 'Submit'
    }
};