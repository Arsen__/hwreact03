import React from 'react';
import logo from './logo.svg';
import './App.css';
import OrderForm from './client/orderForm'

function App() {
  return (
      <OrderForm />
  );
}

export default App;
